module gitlab.com/pages/hugo

go 1.20

require (
	github.com/miguelsimoni/hugo-initio v0.0.0-20230619214421-1f75a080452e // indirect
	github.com/theNewDynamic/gohugo-theme-ananke v0.0.0-20230203204610-a1a99cf12681 // indirect
)
